class Arbre:
    def __init__(self, valeur, enfants=[]):
        self.valeur = valeur
        self.enfants = enfants

    def ajouter_enfant(self, enfant):
        self.enfants.append(enfant)


a1 = Arbre(1)
a2 = Arbre(2)
a1.ajouter_enfant(Arbre(3))
print(a2.enfants)
