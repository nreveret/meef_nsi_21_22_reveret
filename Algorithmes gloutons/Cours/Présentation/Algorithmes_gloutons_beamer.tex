\documentclass{beamer}

\title[Algorithmes gloutons]{Algorithmes gloutons}
\subtitle{MEEF - NSI}
\institute{UCO}
\titlegraphic{\vspace{1.5\baselineskip}\includegraphics[scale=0.08]{glouton2}}
\author{N. Revéret}
\date{5 janvier 2022}

% \usetheme{focus}
\usetheme[numbering=progressbar]{focus} % nofirafonts
\definecolor{main}{RGB}{92, 138, 168}
\definecolor{background}{RGB}{250, 250, 250}
\footlineinfo{Algorithmes gloutons}
\setbeamertemplate{itemize/enumerate body begin}{\vspace{1\baselineskip}}


%-------------------------------------------------------
% Les packages
%-------------------------------------------------------
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage[french]{babel}
\addto\captionsfrench{\def\tablename{Tableau}}
\addto\captionsfrench{\def\figurename{Figure}}
\usepackage{lmodern}
\usepackage{hyperref} % liens hypertextes
\usepackage{graphicx} % insertion d'images
\usepackage{booktabs} % Les tableaux
\usepackage{amssymb} % symboles maths
\usepackage{amsmath} % symboles maths
\usepackage{multicol} % les colonnes
\usepackage[normalem]{ulem} % texte barré
% \usepackage{float} % positionnement des tableaux et autres flottants

%-------------------------------------------------------
% Figure geogebra -> tikz
%-------------------------------------------------------
\usepackage{pgfplots}
\pgfplotsset{compat=1.15}
\usepackage{mathrsfs}
\usetikzlibrary{arrows, positioning, calc, snakes}

%-------------------------------------------------------
% Longueurs personnelles
%-------------------------------------------------------
% \setlength{\textfloatsep}{0.1cm}
% \setlength{\parskip}{6pt}
% \setlength{\parindent}{15pt}
% \setlength{\textfloatsep}{10pt plus 1.0pt minus 2.0pt}
% \setlength{\floatsep}{10pt plus 1.0pt minus 2.0pt}
% \setlength{\intextsep}{10pt plus 1.0pt minus 2.0pt}


%-------------------------------------------------------
% Dossiers contenant les images
%-------------------------------------------------------
\graphicspath{ 
	{../images/}
}

%-------------------------------------------------------
% Mise en forme du code
%-------------------------------------------------------
\usepackage[newfloat=false]{minted}
\setminted{autogobble, tabsize=4, breaklines, breakafter=_, linenos, resetmargins, frame=leftline}
% Environnement python code
\newminted{python}{} % permet d'utiliser \begin{pythoncode} pour écrire du python

% Environnement mintinlinepy
\newmintinline[mintinlinepy]{python}{breaklines, breakafter=_} % permet d'utiliser \mintinlinepy{...} pour écrire du python en ligne
    
% Environnement textcode
\newminted{text}{} % permet d'utiliser \begin{textcode} pour écrire du code en français

% Environnement textcode
\newmintinline[mintinlinetext]{text}{breaklines, breakafter=_} % permet d'utiliser \mintinlinetext{...} pour écrire du code en ligne en français

%-------------------------------------------------------
% Commandes personnelles
%-------------------------------------------------------
\newcommand{\python}{\texttt{python}}

%-------------------------------------------------------
% Le document
%-------------------------------------------------------
\begin{document}

\begin{frame}
  \titlepage
\end{frame}

\section*{I. Complexité des algorithmes}
\subsection{Tri par insertion}
\begin{frame}{\insertsubsection}
  \centering
  {\setlength{\fboxsep}{0pt}
    \setlength{\fboxrule}{1pt}
    \fbox{%
      \includegraphics[width=6cm]{insertion_cards_Cormen}%
    }}

  Le tri du joueur de cartes
\end{frame}

\defverbatim[colored]\codeInsertion{
  \begin{minted}{python}
def triInsertion(A : list) -> None :
  """
  Trie la liste A par insertion
  Le tri est en place
  """
  for i in range(1, len(A)) :
    elt = A[i]
    j = i
    while j > 0 and A[j-1] > elt :
      A[j] = A[j-1]
      j -= 1
    A[j] = elt
      \end{minted}
}

\begin{frame}{\insertsubsection}
  \codeInsertion
\end{frame}

\begin{frame}{\insertsubsection}
  \centering
  {\setlength{\fboxsep}{0pt}
    \setlength{\fboxrule}{1pt}
    \fbox{%
      \includegraphics[width=8cm]{insertion-sort}%
    }}
  \begin{itemize}
    \item Nombre de permutations : $1+2+3+\dots+(n-1)=\frac{n(n-1)}{2}$ \pause
          \vspace{\baselineskip}
    \item Complexité quadratique $\mathcal{O}(n^2)$
  \end{itemize}

\end{frame}

\subsection{Tri fusion}
\begin{frame}{\insertsubsection}
  \centering
  {\setlength{\fboxsep}{0pt}
    \setlength{\fboxrule}{1pt}
    \fbox{%
      \includegraphics[width=5cm]{merge_sort.png}%
    }}
\end{frame}


\defverbatim[colored]\codeTriFusion{
  \begin{minted}[fontsize=\tiny]{py}
def triFusion(T: list) -> list:
    # Cas de base
    if len(T) <= 1:
        return T
    else:
        # L'indice du "milieu" du tableau
        milieu = len(T) // 2

        # Les sous-tableaux de gauche et de droite
        gauche = []
        droite = []
        # On copie la première moitié de T dans gauche,
        # La seconde dans droite
        for i in range(len(T)):
            if i < milieu:
                gauche.append(T[i])
            else:
                droite.append(T[i])

        gauche = triFusion(gauche)
        droite = triFusion(droite)

        # On renvoie leur fusion
        return fusion(gauche, droite)
  \end{minted}
}

\defverbatim[colored]\codeFusion{
  \begin{minted}[fontsize=\footnotesize]{py}
def fusion(G: list, D: list) -> list:
    R = []  # Le tableau qui sera renvoyé

    # On ajoute "infini" à la fin des tableaux pour simplifier la fusion
    G.append(float('inf'))
    D.append(float('inf'))

    while len(G) > 1 or len(D) > 1:  # les seuls éléments restants seront les infinis
        if G[0] <= D[0]:
            R.append(G.pop(0))
        else:
            R.append(D.pop(0))

    return R
  \end{minted}
}

\begin{frame}{\insertsubsection}
  \codeTriFusion{}
\end{frame}

\begin{frame}{\insertsubsection}
  \codeFusion{}
\end{frame}

\begin{frame}{\insertsubsection}
  \centering{
    \setlength{\fboxsep}{0pt}
    \setlength{\fboxrule}{1pt}
    \fbox{%
      \includegraphics[width=10cm]{complexite_fusion.png}%
    }
  }

  \begin{itemize}
    \item $\log_2(n)$ étapes de divisions
  \end{itemize}

\end{frame}

\begin{frame}{\insertsubsection}
  \centering
  {\setlength{\fboxsep}{0pt}
    \setlength{\fboxrule}{1pt}
    \fbox{%
      \includegraphics[width=6cm]{complexite_fusion2.png}%
    }}

  \begin{itemize}
    \item $\log_2(n)$ étapes de fusion manipulant chacune $n$ valeurs \pause
          \vspace{\baselineskip}
    \item Complexité semi-logarithmique $\mathcal{O}(n\log (n))$
  \end{itemize}

\end{frame}

\subsection{Classes de complexité}
\begin{frame}{\insertsubsection}
  \centering
  {\setlength{\fboxsep}{0pt}
    \setlength{\fboxrule}{1pt}
    \fbox{%
      \includegraphics[width=10cm]{comparaisons_fonctions.PNG}%
    }}

  \begin{itemize}
    \item<1> $f(n) = \Theta (g(n))$ : $f$ est \og encadrée \fg\ par $g$ (à des coefficients près)
      % \vspace{\baselineskip}
    \item<2> $f(n) = \mathcal{O}(g(n))$ : $f$ est \og dominée \fg\ par $g$ (à un coefficient près)
      % \vspace{\baselineskip}
    \item<3> $f(n) = \Omega (g(n))$ : $f$ \og domine \fg\ $g$ (à un coefficient près)
      % \vspace{\baselineskip}
  \end{itemize}

\end{frame}

\begin{frame}{\insertsubsection}
  \centering
  {\setlength{\fboxsep}{0pt}
    \setlength{\fboxrule}{1pt}
    \fbox{%
      \includegraphics[width=9cm]{complexites.png}%
    }}
\end{frame}

\begin{frame}{\insertsubsection}
  \centering
  {\setlength{\fboxsep}{0pt}
    \setlength{\fboxrule}{1pt}
    \fbox{%
      \includegraphics[width=9cm]{sort_complexities.png}%
    }}
\end{frame}

\begin{frame}{\insertsubsection}
  \centering
  {\setlength{\fboxsep}{0pt}
    \setlength{\fboxrule}{1pt}
    \fbox{%
      \includegraphics[width=11cm]{adt_complexities.jpg}%
    }}
\end{frame}

\section*{II. Approche gloutonne}
\subsection{Cadre d'utilisation}
\begin{frame}{\insertsubsection}
  \centering
  {\Large{
      Recherche d'optimum (maximum ou minimum)
    }}
  \vspace{\baselineskip}

  {\setlength{\fboxsep}{0pt}
    \setlength{\fboxrule}{1pt}
    \fbox{%
      \includegraphics[width=8cm, height=4cm]{gradient.jpg}}%
  }
\end{frame}

\begin{frame}{\insertsubsection}
  \centering
  {\Large{
      Choix gloutons
    }}
  \vspace{\baselineskip}

  \begin{figure}
    \centering
    \begin{tikzpicture}
      \draw[rounded corners] (0, 0) rectangle (10, 5) {};
      \node[draw=none, anchor=north west] at (0,5) (N) {Taille $N$};

      \draw[rounded corners, fill=lightgray!25] (0.25, 0.25) rectangle (9.75, 3.5) {};
      \node[draw=none, anchor=north west] at (0.5,3.5) (N1) {Taille $N-1$};

      \draw[rounded corners, fill=lightgray!50] (0.5, 0.5) rectangle (9.5, 2) {};
      \node[draw=none, anchor=north west] at (0.75,2) (N2) {Taille $N-2$};

      \node[draw=none, anchor=north] at ($(N2.south)+(0,-0.5)$) (N3) {\small{\textit{$\dots$}}};

      \draw[-latex, thick] (N.south) -- ($(N.south)+(0,-0.9)$) node [midway, right, draw=none, rectangle] {\small{\textit{Optimum local}}};
      \draw[-latex, thick] (N1.south) -- ($(N1.south)+(0,-0.9)$) node [midway, right, draw=none, rectangle] {\small{\textit{Optimum local}}};
      \draw[-latex, thick] (N2.south) -- ($(N2.south)+(0,-0.5)$) node [midway, right, draw=none, rectangle] {\small{\textit{$\dots$}}};
    \end{tikzpicture}
  \end{figure}
\end{frame}

\begin{frame}{\insertsubsection}

  \centering
  {\Large{
      Deux possibilités
    }}
  % \vspace{\baselineskip}

  \vspace{\baselineskip}
  \begin{itemize}

    \item \alt<1>{La succession des choix locaux optimums \textbf{est} optimale}{\sout{La succession des choix locaux optimums \textbf{est} optimale}}
          \vspace{\baselineskip}
    \item<2-> La succession des choix locaux optimums \textbf{n'est pas} optimale
      \vspace{0.8\baselineskip}
      \begin{itemize}
        \item<3-> Intéressant tout de même si le coût de l'algo. est bas et la solution est \og satisfaisante \fg
      \end{itemize}
  \end{itemize}
\end{frame}

\subsection{Exemple : Planification}
\begin{frame}{\insertsubsection}
  \begin{itemize}
    \item $N$ tâches
    \item $t_0, \dots, t_{N-1}$ durées
    \item $d_0, \dots, d_{N-1}$ heures de rendus maximales (\textit{due times})
          \item<2->{\color{red} On souhaite attribuer à chaque tâche une heure de début ($s_i$) afin de \textbf{minimiser le retard maximal}}
  \end{itemize}

  \vspace{1\baselineskip}
  \begin{figure}[ht]
    \centering
    \definecolor{ffccww}{rgb}{1.,0.8,0.4}
    \definecolor{ffffff}{rgb}{1.,1.,1.}
    \begin{tikzpicture}[line cap=round,line join=round,>=triangle 45,x=0.8cm,y=0.8cm, line width=1pt]
      \clip(0.3,-0.3) rectangle (7.7,3.5);
      \draw[fill=ffffff,fill opacity=0.75] (1.,2.5) rectangle (2.5,2.);
      \node[rectangle, draw=none, anchor=south] at (1,2.5) {$d_i-t_i$};
      \node[rectangle, draw=none, anchor=south] at (2.5,2.5) {$d_i$};
      \draw [-latex,line width=1pt] (1.5,2.25) -- (1,2.25);
      \draw [-latex,line width=1pt] (2,2.25) -- (2.5,2.25);
      \node[rectangle, draw=none] at (1.75,2.25) {$t_i$};
      \draw[fill=ffffff,fill opacity=0.75] (2.5,1.5) rectangle (4.5,1.);
      \draw[fill=ffffff,fill opacity=0.75] (3.,2.5) rectangle (4.,2.);
      \draw[fill=ffffff,fill opacity=0.75] (5.,2.5) rectangle (7.5,2.);
      \draw[fill=ffffff,fill opacity=0.75] (5.,1.5) rectangle (6.5,1.);
      \draw[fill=ffffff,fill opacity=0.75] (2.,1.5) rectangle (0.75,1.);
      \draw [-latex,line width=1pt] (0.5,0.5) -- (7.5,0.5);
      \draw (4,0.4) node[anchor=north] {$temps$};
    \end{tikzpicture}
  \end{figure}

\end{frame}

\begin{frame}{\insertsubsection}
  \begin{itemize}
    \item Retard de la tâche $i$ : $$r_i= \max(0,fin_i - rendu_i)=max(0,e_i-d_i)$$
          \vspace{1\baselineskip}
    \item Retard maximal : $$\max_{0 \le k < N} r_k$$
  \end{itemize}

\end{frame}

\begin{frame}{\insertsubsection}
  \centering
  \begin{table}
    \begin{tabular}{lccccccc}
            & 0          & 1 & 2 & 3  & 4  & 5  \\\midrule
      $t_i$ & 3          & 2 & 1 & 4  & 3  & 2  \\
      $d_i$ & \textbf{6} & 8 & 9 & 9  & 14 & 15 \\
      \midrule
      $s_i$ & \textbf{5}          & 1 & 0 & 11 & 8  & 3  \\
      \bottomrule
    \end{tabular}
  \end{table}

  \begin{figure}[ht]
    \definecolor{ffccww}{rgb}{1.,0.8,0.4}
    \definecolor{ffffff}{rgb}{1.,1.,1.}
    \begin{tikzpicture}[line width=1pt, x=0.7cm, y=0.8cm]
      % \clip(0.3,-0.3) rectangle (7.7,3);
      \draw[fill=ffffff,fill opacity=0.75] (0,0) rectangle (0.9,1);
      \node[rectangle, draw=none, anchor=west] at (0,0.5) {$t_2$};
      \draw[fill=ffffff,fill opacity=0.75] (1,0) rectangle (2.9,1);
      \node[rectangle, draw=none, anchor=west] at (1,0.5) {$t_1$};
      \draw[fill=ffffff,fill opacity=0.75] (3,0) rectangle (4.9,1);
      \node[rectangle, draw=none, anchor=west] at (3,0.5) {$t_5$};
      \draw[fill=lightgray!50,fill opacity=0.75] (5,0) rectangle (7.9,1);
      \node[rectangle, draw=none, anchor=west] at (5,0.5) {$t_0$};
      \draw[fill=ffffff,fill opacity=0.75] (8,0) rectangle (10.9,1);
      \node[rectangle, draw=none, anchor=west] at (8,0.5) {$t_4$};
      \draw[fill=ffffff,fill opacity=0.75] (11,0) rectangle (14.9,1);
      \node[rectangle, draw=none, anchor=west] at (11,0.5) {$t_3$};
      \draw [line width=1pt, snake=ticks,segment length=0.7cm] (0,-0.5) -- (15,-0.5);
      \draw [-latex,line width=1pt] (-0.5,-0.5) -- (15.5,-0.5);
      \node[draw=none, anchor=north] at  (0,-0.5) {\small{0}};
      \node[draw=none, anchor=north] at  (1,-0.5) {\small{1}};
      \node[draw=none, anchor=north] at  (2,-0.5) {\small{2}};
      \node[draw=none, anchor=north] at  (3,-0.5) {\small{3}};
      \node[draw=none, anchor=north] at  (4,-0.5) {\small{4}};
      \node[draw=none, anchor=north] at  (5,-0.5) {\small{5}};
      \node[draw=none, anchor=north] at  (6,-0.5) {\small{6}};
      \node[draw=none, anchor=north] at  (7,-0.5) {\small{7}};
      \node[draw=none, anchor=north] at  (8,-0.5) {\small{\textbf{8}}};
      \node[draw=none, anchor=north] at  (9,-0.5) {\small{9}};
      \node[draw=none, anchor=north] at  (10,-0.5) {\small{10}};
      \node[draw=none, anchor=north] at  (11,-0.5) {\small{1}1};
      \node[draw=none, anchor=north] at  (12,-0.5) {\small{12}};
      \node[draw=none, anchor=north] at  (13,-0.5) {\small{13}};
      \node[draw=none, anchor=north] at  (14,-0.5) {\small{14}};
      \node[draw=none, anchor=north] at  (15,-0.5) {\small{15}};
    \end{tikzpicture}
  \end{figure}

  $t_0$ est due à $6h$ au plus tard, débute à $5h$ et terminée à $5+3=8h$ : $r_0=2$
\end{frame}

\begin{frame}{\insertsubsection}
  \centering
  {\Large Première Approche}

  \vfill
  On traite les tâches par durées croissantes...
  \vfill

  \begin{table}
    \begin{tabular}{lcc}
            & 0  & 1 \\\midrule
      $t_i$ & 1  & 5 \\
      $d_i$ & 20 & 5 \\
      \midrule
      \only<2>{
      $s_i$ & 0  & 1 \\
        \bottomrule
      }
    \end{tabular}
  \end{table}

  \only<2>{\textbf{Mauvaise approche !}}
\end{frame}

\begin{frame}{\insertsubsection}
  \centering
  {\Large Deuxième Approche}

  \vfill
  On traite les tâches par \og délais \fg\ ($d_i-t_i$) croissants...
  \vfill

  \begin{table}
    \begin{tabular}{lcc}
            & 0 & 1 \\\midrule
      $t_i$ & 5 & 1 \\
      $d_i$ & 5 & 2 \\
      \midrule
      \only<2>{
      $s_i$ & 0 & 5 \\
        \bottomrule
      }
    \end{tabular}
  \end{table}

  \only<2>{\textbf{Mauvaise approche !}}
\end{frame}

\defverbatim[colored]\codePlanning{
  \begin{minted}[fontsize=\footnotesize]{text}
Fonction planning(tableau rendus, tableau durées) :
    Trier le tableau rendus dans l'ordre croissant (garder trace des indices)
    Trier le tableau durées dans le même ordre

    Créer un tableau débuts contenant N cellules vides

    heure = 0

    Pour chaque tâche :
        i est l'indice de cette tâche
        début[i] = heure
        heure = heure + durées[i]

    Renvoyer débuts
\end{minted}
}

\begin{frame}{\insertsubsection}
  \centering
  {\Large Approche correcte }

  \vfill
  On traite les tâches par heures de rendus croissantes
  \vfill

  \codePlanning

\end{frame}

\begin{frame}{\insertsubsection}
  \centering
  \begin{table}
    \begin{tabular}{lccccccc}
                      & 0 & 1 & 2 & 3                  & 4  & 5  \\\midrule
      $t_i$           & 3 & 2 & 1 & 4                  & 3  & 2  \\
      $d_i$           & 6 & 8 & 9 & 9                  & 14 & 15 \\
      \midrule
      \only<2->{$s_i$ & 0 & 3 & 5 & 6                  & 10 & 13 \\
        \bottomrule
      }
      \only<3->{$r_i$ & 0 & 0 & 0 & \textcolor{red}{1} & 0  & 0  \\
        \bottomrule
      }
    \end{tabular}
  \end{table}

\end{frame}


\begin{frame}{\insertsubsection}
  \centering
  {\Large Analyse }

  \vfill
  \begin{itemize}
    \item<1-> L'algorithme se termine du fait de la boucle \og \texttt{Pour} \fg
      \vfill
    \item<2-> L'algorithme assure que les tâches ne s'exécutent pas en même temps (\texttt{temps = temps + durées[i]})
      \vfill
    \item<3-> La solution est-elle \textbf{optimale} ?
  \end{itemize}
\end{frame}

\begin{frame}{\insertsubsection}
  \centering
  {\Large\textbf{Quelques remarques}}

  \vfill
  \begin{itemize}
    \item<1-> Il est toujours possible de faire en sorte que le planning s'exécute sans temps morts
  \end{itemize}

  \vfill
  \begin{figure}[ht]
    \definecolor{ffffff}{rgb}{1.,1.,1.}
    \begin{tikzpicture}[line width=1pt, x=0.7cm, y=0.8cm]
      % \clip(0.3,-0.3) rectangle (7.7,3);
      \draw[fill=ffffff,fill opacity=0.75] (0,0) rectangle (0.9,1);
      \draw[fill=ffffff,fill opacity=0.75] (2,0) rectangle (3.9,1);
      \draw[fill=ffffff,fill opacity=0.75] (4,0) rectangle (4.9,1);
      \draw [line width=1pt, snake=ticks,segment length=0.7cm] (0,-0.5) -- (5,-0.5);
      \draw [-latex, line width=1pt] (-0.5,-0.5) -- (5.5,-0.5);

      \draw[fill=ffffff,fill opacity=0.75] (8,0) rectangle (8.9,1);
      \draw[fill=ffffff,fill opacity=0.75] (9,0) rectangle (10.9,1);
      \draw[fill=ffffff,fill opacity=0.75] (11,0) rectangle (11.9,1);
      \draw [line width=1pt, snake=ticks,segment length=0.7cm] (8,-0.5) -- (13,-0.5);
      \draw [-latex, line width=1pt] (7.5,-0.5) -- (13.5,-0.5);
      \draw [-latex, line width=1pt, dashed] (2,0.5) -- (0.9,0.5);
    \end{tikzpicture}
  \end{figure}

\end{frame}

\begin{frame}{\insertsubsection}
  \centering
  {\Large\textbf{Quelques remarques}}

  \vfill
  \begin{itemize}
    \item<1-> Il est possible d'échanger l'ordre de deux tâches consécutives sans que le planning ne devienne incorrect
  \end{itemize}

  \vfill
  \begin{figure}[ht]
    \definecolor{ffffff}{rgb}{1.,1.,1.}
    \begin{tikzpicture}[line width=1pt, x=0.7cm, y=0.8cm]
      \draw[fill=ffffff,fill opacity=0.75] (0,0) rectangle (0.9,1);
      \draw[fill=red!50,fill opacity=0.75] (1,0) rectangle (3.9,1);
      \draw[fill=green!50,fill opacity=0.75] (4,0) rectangle (4.9,1);
      \draw [line width=1pt, snake=ticks,segment length=0.7cm] (0,-0.5) -- (5,-0.5);
      \draw [-latex, line width=1pt] (-0.5,-0.5) -- (5.5,-0.5);
      \draw [latex-latex, dashed] (2.5,1) to [out=90,in=90] (4.5,1);
      \node[rectangle, draw=none] at (2.5,0.5) {$j$};
      \node[rectangle, draw=none] at (4.5,0.5) {$i$};

      \draw[fill=ffffff,fill opacity=0.75] (7,0) rectangle (7.9,1);
      \draw[fill=green!50,fill opacity=0.75] (8,0) rectangle (8.9,1);
      \draw[fill=red!50,fill opacity=0.75] (9,0) rectangle (11.9,1);
      \draw [line width=1pt, snake=ticks,segment length=0.7cm] (7,-0.5) -- (12,-0.5);
      \draw [-latex, line width=1pt] (6.5,-0.5) -- (12.5,-0.5);
      \node[rectangle, draw=none] at (8.5,0.5) {$i$};
      \node[rectangle, draw=none] at (10.5,0.5) {$j$};

    \end{tikzpicture}
  \end{figure}

\end{frame}

\begin{frame}{\insertsubsection}
  \centering
  {\Large\textbf{Quelques remarques sur les inversions}}

  \vfill
  \begin{itemize}
    \item<1-> On appelle \textbf{inversion}, deux tâches qui s'exécutent dans l'ordre inverse de leurs heures de rendus
      \vfill
    \item<1-> $Rendu_i < Rendu_j$ et pourtant $D\acute{e}but_i > D\acute{e}but_j$
      \vfill
    \item<2-> Le planning obtenu avec l'algorithme glouton n'a \textcolor{red}{jamais} d'inversions
  \end{itemize}

\end{frame}

\begin{frame}{\insertsubsection}
  \centering
  {\Large\textbf{Quelques remarques sur les inversions}}

  \vfill
  \begin{itemize}
    \item Dans un planning sans temps morts, s'il existe des inversions, au moins une d'entre elles prend place entre deux tâches programmées successivement
  \end{itemize}
  \vfill

\end{frame}

\begin{frame}{\insertsubsection}
  \centering
  {\Large\textbf{Quelques remarques sur les inversions}}

  \vfill
  \begin{itemize}
    \item Échanger l'ordre de deux tâches consécutives \og inversées \fg\ n'augmente pas le retard maximal
          \vfill
          \only<2->{
            \begin{itemize}
              \item<2> Les tâches non concernées par l'inversion ne sont pas modifiées
                \vfill
              \item<3> La tâche $i$ est programmée plus tôt : son retard ne peut que diminuer (ou rester constant)
            \end{itemize}
          }
  \end{itemize}

  \vfill
  \begin{figure}[ht]
    \definecolor{ffffff}{rgb}{1.,1.,1.}
    \begin{tikzpicture}[line width=1pt, x=0.7cm, y=0.8cm]
      \draw[fill=ffffff,fill opacity=0.75] (0,0) rectangle (0.9,1);
      \draw[fill=red!50,fill opacity=0.75] (1,0) rectangle (3.9,1);
      \draw[fill=green!50,fill opacity=0.75] (4,0) rectangle (4.9,1);
      \draw [line width=1pt, snake=ticks,segment length=0.7cm] (0,-0.5) -- (5,-0.5);
      \draw [-latex, line width=1pt] (-0.5,-0.5) -- (5.5,-0.5);
      \draw [latex-latex, dashed] (2.5,1) to [out=90,in=90] (4.5,1);
      \node[rectangle, draw=none] at (0.5,0.5) {$k$};
      \node[rectangle, draw=none] at (2.5,0.5) {$j$};
      \node[rectangle, draw=none] at (4.5,0.5) {$i$};


      \draw[fill=ffffff,fill opacity=0.75] (7,0) rectangle (7.9,1);
      \draw[fill=green!50,fill opacity=0.75] (8,0) rectangle (8.9,1);
      \draw[fill=red!50,fill opacity=0.75] (9,0) rectangle (11.9,1);
      \draw [line width=1pt, snake=ticks,segment length=0.7cm] (7,-0.5) -- (12,-0.5);
      \draw [-latex, line width=1pt] (6.5,-0.5) -- (12.5,-0.5);
      \node[rectangle, draw=none] at (7.5,0.5) {$k$};
      \node[rectangle, draw=none] at (8.5,0.5) {$i$};
      \node[rectangle, draw=none] at (10.5,0.5) {$j$};

    \end{tikzpicture}
  \end{figure}
\end{frame}

\begin{frame}{\insertsubsection}
  \centering
  {\Large\textbf{Quelques remarques sur les inversions}}

  \vfill
  \begin{itemize}
    \item Échanger l'ordre de deux tâches consécutives \og inversées \fg\ n'augmente pas le retard maximal
          \begin{itemize}
            \item La tâche $j$ est programmée plus tard. On note $r_j$ son retard \textbf{avant} l'échange et $r'_j$ celui \textbf{après}
          \end{itemize}
  \end{itemize}
  \vspace{-0.2\baselineskip}
  \begin{equation*}
    \begin{split}
      r'_j & = e'_j-d_j \\
      & = e_i-d_j \\
      & \le e_i-d_i \text{ (la tâche i est due avant)} \\
      & \le r_i \\
    \end{split}
  \end{equation*}

  \begin{figure}[ht]
    \definecolor{ffffff}{rgb}{1.,1.,1.}
    \begin{tikzpicture}[line width=1pt, x=0.7cm, y=0.8cm]
      \draw[fill=ffffff,fill opacity=0.75] (0,0) rectangle (0.9,1);
      \draw[fill=red!50,fill opacity=0.75] (1,0) rectangle (3.9,1);
      \draw[fill=green!50,fill opacity=0.75] (4,0) rectangle (4.9,1);
      \draw [line width=1pt, snake=ticks,segment length=0.7cm] (0,-0.5) -- (5,-0.5);
      \draw [-latex, line width=1pt] (-0.5,-0.5) -- (5.5,-0.5);
      \draw [latex-latex, dashed] (2.5,1) to [out=90,in=90] (4.5,1);
      \node[rectangle, draw=none] at (0.5,0.5) {$k$};
      \node[rectangle, draw=none] at (2.5,0.5) {$j$};
      \node[rectangle, draw=none] at (4.5,0.5) {$i$};


      \draw[fill=ffffff,fill opacity=0.75] (7,0) rectangle (7.9,1);
      \draw[fill=green!50,fill opacity=0.75] (8,0) rectangle (8.9,1);
      \draw[fill=red!50,fill opacity=0.75] (9,0) rectangle (11.9,1);
      \draw [line width=1pt, snake=ticks,segment length=0.7cm] (7,-0.5) -- (12,-0.5);
      \draw [-latex, line width=1pt] (6.5,-0.5) -- (12.5,-0.5);
      \node[rectangle, draw=none] at (7.5,0.5) {$k$};
      \node[rectangle, draw=none] at (8.5,0.5) {$i$};
      \node[rectangle, draw=none] at (10.5,0.5) {$j$};

    \end{tikzpicture}
  \end{figure}
\end{frame}

\begin{frame}{\insertsubsection}
  \centering
  {\Large\textbf{Preuve de l'optimalité}}

  \vfill
  \begin{itemize}
    \item Soit $G$ un planning obtenu à l'aide de l'algorithme glouton
    \item Soit $Opt$ un planning optimal sans temps mort
  \end{itemize}

  \begin{itemize}
    \item<2-> Si $Opt$ ne contient aucune inversion alors $G=Opt$
    \item <3-> Si $Opt$ contient une inversion, échanger deux tâches consécutives inversées ne diminue par le retard maximal...
    \item <4-> ... mais fait que $G$ et $Opt$ se ressemblent une peu plus (moins d'inversions)
    \item <5-> On réitère le processus jusqu'à avoir $G=Opt$
  \end{itemize}

\end{frame}

\section*{III. Application}
\subsection{\textit{Bin-Packing}}
\begin{frame}{\insertsubsection}
  \centering
  \begin{itemize}
    \item $N$ objets de poids $P_0, \dots, P_{N-1}$
    \item des boîtes de poids maximal $P_{maxi}$
  \end{itemize}
  \vfill
  {\setlength{\fboxsep}{0pt}
    \setlength{\fboxrule}{1pt}
    \fbox{%
      \includegraphics[width=6cm]{binpacking.jpg}%
    }}

  \begin{itemize}
    \item<2-> Comment ranger les objets dans un minimum de boîtes ?
  \end{itemize}
\end{frame}

\begin{frame}{\insertsubsection}
  \centering
  {
    \Large{Première position}
  }

  \vfill
  \begin{itemize}
    \item On place chaque élément dans la première boîte qui peut le contenir...
    \item Si aucune boîte ne peut le contenir, on en utilise une nouvelle
  \end{itemize}
  \vfill
\end{frame}

\begin{frame}{\insertsubsection}
  \centering
  {
    \Large{Meilleure position}
  }

  \vfill
  \begin{itemize}
    \item On place chaque élément dans la meilleure boîte qui peut le contenir...
    \item Par meilleure on entend celle dans laquelle il laisse un minimum de place (la plus \og pleine \fg)
    \item Si aucune boîte ne peut le contenir, on en utilise une nouvelle
  \end{itemize}
  \vfill
\end{frame}

\begin{frame}{\insertsubsection}
  \centering
  {
    \Large{Questions}
  }

  $$Poids = [2, 5, 4, 7, 1, 3, 8]$$
  $$P_{maxi} = 10$$

  \vfill
  \begin{itemize}
    \item<1-> Appliquer chaque méthode
    \item<2-> Les méthodes sont-elles optimales ?
    \item<3-> Coût de chaque méthode ? 
    \item<4-> Et si l'on trie les objets avant la répartition ? (efficacité, coût...) 
  \end{itemize}
  \vfill
\end{frame}

\begin{frame}{\insertsubsection}
  \centering
  {
    \Large{Codage}
  }

  \vfill
  \begin{itemize}
    \item<1-> \texttt{nObjets(n : int, pMaxi : int) -> int} : crée une liste contenant les poids des objets
    \item<2-> \texttt{premierePosition(objets : list, pMaxi : int) -> list} : répartit les objets dans une liste de liste/boîtes
    \item<3-> \texttt{meilleurePosition(objets : list, pMaxi : int) -> list} : répartit les objets dans une liste de liste/boîtes
  \end{itemize}
  \vfill
\end{frame}

\end{document}